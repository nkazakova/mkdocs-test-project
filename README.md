# Как создать сайт на GitLab Pages через mkdocs

## Локальный сайт

1. Устанавливаем Python v3.8 и выше
1. Устанавливаем pip и затем mkdocs. Детально тут: [MkDocs Installation](https://www.mkdocs.org/user-guide/installation/).
1. Следуем инструкции на [mkdocs.org](https://www.mkdocs.org/getting-started/): 

    ```
    mkdocs new my-project
    cd my-project
    mkdocs serve
    ```

Сайт начинает работать на `localhost`. Добавляем и меняем `.md` файлы, сайт автоматически пересобирается после сохранения изменений.

![localhost-serve](readme-img/localhost-serve.png)

Настройки mkdocs содержатся в `mkdocs.yml`. Там можно поставить [плагины](https://www.mkdocs.org/user-guide/configuration/#plugins), [расширения для маркдауна](https://www.mkdocs.org/user-guide/configuration/#markdown_extensions), поменять тему, сделать навигацию и т.д. 

Здесь установлена тема [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/getting-started/).

> Для локальной сборки плагины должны быть установлены **локально** (например, `pip install mkdocs-mermaid2-plugin`). Список плагинов: [MkDocs-Plugins](https://github.com/mkdocs/mkdocs/wiki/MkDocs-Plugins).

## GitLab Pages

1. Создайте репозиторий на GitLab. Имя репозитория будет использовано в URL: `https://{profile-name}.gitlab.io/{repo-name}`.
1. Настройте файл [mkdocs.yml](mkdocs.yml), хранящий настройки движка [mkdocs](https://www.mkdocs.org/getting-started/):

    - Укажите `site_name`.
    - Укажите `site_dir`. Его нужно будет передать в настройки CI/CD. В этом репозитории используется значение `public`. 
    - Опционально. Добавьте навигацию, укажите тему (`material` или другую), включите расширения и плагины (например, поиск, время на чтение статьи, дата последнего изменения, якоря на заголовках и еще много разного).

1. Настройте файл [.gitlab-ci.yml](.gitlab-ci.yml) для [CI/CD](https://docs.gitlab.com/ee/ci/). Настройки этой репы:

    - Использовать докер-образ alpine для сборки сайта.
	- Установить плагины из файла `requirements.txt` перед сборкой докер-образа.
    - Запускать stage test для все коммитов не в дефолтную ветку.
    - Запускать stage build для всех коммитов в дефолтную ветку.
    - Путь билдежки (`paths`) совпадает со значением `public`, указанной в `mkdocs.yml`.
    - Используется [кэш](https://docs.gitlab.com/ee/ci/caching/) для уменьшения времени билдежки.

1. Добавьте плагины, используемые в `mkdocs.yml`, в файл [requirements.txt](requirements.txt).

В итоге в репозитории содержатся следующие файлы (необходимый минимум):

- `mkdocs.yml` - настройки mkdocs
- `.gitlab-ci.yml` - настройки CI/CD
- `requirements.txt` - файл, который хранит список плагинов для докер-образа
- папка `docs` с контентом (`.md` файлами и картинками)

На каждый коммит в дефолтную ветку запускается пайплайн с джобом build. Сайт собирается после успешного прохождения пайплайна:

![job-succeeded](readme-img/job-succeeded.png)

После этого в **Settings** → **Pages** можно увидеть URL своего сайта:

```
Your pages are served under:

https://nkazakova.gitlab.io/test-project
```

Сайт будет автоматически обновляться после успешного прохождения билдежки.