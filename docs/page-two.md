# Page Two

## Blockquote

> In ancient times cats were worshipped as gods; they have not forgotten this.

## Mermaid Diagram

Typical cat behavior:

```mermaid
%%{init: {'theme':'default'}}%%
stateDiagram-v2
    [*] --> Still
    Still --> [*]
    Still --> Moving
    Moving --> Still
    Moving --> Crash
    Crash --> [*]
```

<!-- You can also write comments. They would not appear on the page. -->

## Code Snippets

```javascript
var s = "JavaScript syntax highlighting";
alert(s);
```

```python
def function():
    #indenting works just fine in the fenced code block
    s = "Python syntax highlighting"
    print s
```

```
No language indicated, so no syntax highlighting.
s = "No highlighting is shown for this line."
But let's throw in a <b>tag</b>.
```