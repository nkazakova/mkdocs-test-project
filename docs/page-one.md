# Page One

## List

This is a list:

- cat
- dog
- turtle


## Image

Inline-style (hover to see title text):

![alt text](img/markdown_logo.png "This is a tooltip")

Reference-style (hover to see title text):

![alt text1][logo]

## Table

| Left Aligned Header | Centered Header | Right Aligned Header |
| :---         | :---:    | ---:          |
| cell 1   | cell 2   | cell 3   |
| cell 4 | cell 5 is longer | cell 6 is much longer than the others, but that's ok. It eventually wraps the text when the cell is too large for the display size. |
| cell 7   |          | cell 9   |



## Mermaid Pie

```mermaid
%%{init: {'theme':'dark'}}%%
pie title Pets adopted by volunteers
    "Dogs" : 286
    "Cats" : 185
    "Turtles" : 15            
```




[logo]: img/markdown_logo.png "This is a tooltip"